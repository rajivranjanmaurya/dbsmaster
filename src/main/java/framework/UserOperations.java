package framework;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class UserOperations {
public WebDriver driver;

public UserOperations(WebDriver driver){
	this.driver = driver;
}

public void inputText(WebElement el, String str){
	el.sendKeys(str);
}

public void clickElement(WebElement el){
	el.click();
}

public void clearText(WebElement el){
	el.clear();
}

}
