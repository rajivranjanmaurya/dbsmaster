package framework;

import org.openqa.selenium.WebDriver;

public class BrowserOperations {
	public WebDriver driver;
	public BrowserOperations(WebDriver driver){
		this.driver = driver;
	}

	public void navigateToApplication(String Url){
		driver.get(Url);
	}
}
