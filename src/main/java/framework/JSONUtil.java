package framework;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JSONUtil {

	 public JSONObject jsonObject;
	 public JSONParser parser = new JSONParser();
	 public String JsonFile ="";
	 public Object obj=null;
	// Set <HashMap <String, String>> hm;
	
	public JSONUtil(String JsonFile){
		JsonFile = System.getProperty("user.dir") + "\\Resources" + "\\" + JsonFile + ".json";
		
		try {
			obj = parser.parse(new FileReader(JsonFile));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		jsonObject = (JSONObject)obj;
	}
	
		
	public String getConfigValue(String property){
		return (String) jsonObject.get(property);
	}	
	}

