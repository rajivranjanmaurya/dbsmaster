package stepDefinitions;

import org.openqa.selenium.WebDriver;

import cucumber.api.java.en.Given;
import framework.BrowserOperations;
import framework.UserOperations;
import helpers.FlipkartHomeHelper;
import pages.FlipkartHomePage;

public class FlipKartHome {
WebDriver driver;
BrowserOperations browserOperations;
FlipkartHomeHelper flipkartHomeHelper;

public FlipKartHome(){
	this.driver = BrowserConfiguration.driver;
	browserOperations = new BrowserOperations(driver);
	flipkartHomeHelper = new FlipkartHomeHelper(driver);
}

@Given("^I am on flipkart home page$")
public void i_am_on_flipkart_homepage() {
	browserOperations.navigateToApplication("https://www.flipkart.com/"); 
    
}

@Given("^I click (.*) section$")
public void i_click_electronic_section(String product) {
	flipkartHomeHelper.selectProduct(product);
    
}

@Given("^Electronic section should be displayed$")
public void electronic_section_displayed() {
	System.out.println("Electronic Section Is Displayed");
    
}
}
