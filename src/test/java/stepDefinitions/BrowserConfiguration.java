package stepDefinitions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import junit.framework.Assert;


public class BrowserConfiguration {

	 public static WebDriver driver = null;
	 public static ExtentReports extent;
	 public static ExtentTest extentTest;
	
	 public static void intentiateBrowser(String browser){
		String driverPath = System.getProperty("user.dir") + "\\Resources" + "\\" + "\\Drivers" + "\\";
		if (browser .contains("chrome")){
			System.setProperty("webdriver.chrome.driver", driverPath + "\\chromedriver.exe" + "\\" );
			driver = new ChromeDriver();
			initializeBrowser(driver);
		}else if (browser.contains("firefox")){
			// C:\Program Files (x86)\Mozilla Firefox\firefox.exe
			//System.setProperty("webdriver.chrome.driver", driverPath + "\\chromedriver.exe" + "\\" );
			driver = new FirefoxDriver();
			initializeBrowser(driver);
		}else if (browser.contains("ie")){
			System.setProperty("webdriver.ie.driver", driverPath + "\\IEDriverServer.exe" + "\\" );
			driver = new InternetExplorerDriver();
			initializeBrowser(driver);
		}else{
			System.out.println("Please provide a valid browser");
		}
	}
	
	@Before
	public  static void intentiateBrowser(Scenario scenario)  {
		extent = new ExtentReports(System.getProperty("user.dir") + "/Reports/Test.html", false);
		extentTest = extent.startTest(scenario.getName());
		//If want to open new browser for eavery scenario then copy paste the code of 	intentiateBrowser(String browser) here		
	}
	
	public static void initializeBrowser(WebDriver driver){
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
	}
	
	
	@After
	public static void reporting(){
		extent.startTest("TestCaseName", "Description");
				//TestCaseName – Name of the test
				//Description – Description of the test
				//Starting test
				Assert.assertTrue(true);
				//To generate the log when the test case is passed
				extentTest.log(LogStatus.PASS, "Test Case Passed is passTest");
				extent.endTest(extentTest);
				extent.flush();
				extent.close();
		//driver.quit();
	}

	
	
}
