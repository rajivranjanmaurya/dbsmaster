package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import framework.UserOperations;

public class FlipkartHomePage {

public WebDriver driver;
public UserOperations userOperations;
	
	public FlipkartHomePage(WebDriver driver){
		this.driver = driver;
		userOperations = new UserOperations(driver);
		
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//span[contains(text(),'Electronics')]")
    public WebElement Electronics;
	
	@FindBy(xpath="//span[contains(text(),'TVs & Appliances')]")
	public WebElement TvApplicances;
	
	@FindBy(xpath="//span[contains(text(),'Men')]")
	public WebElement men;
	
	@FindBy(xpath="//span[contains(text(),'Women')]")
	public WebElement women;
	
	public void clickProductCategory(WebElement el){
		userOperations.clickElement(el);
	}
	
	public void inputText(WebElement el,String st){
		userOperations.inputText(el, st);
	}
	
}
