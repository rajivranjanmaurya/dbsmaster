package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import framework.UserOperations;

public class ElectronicsPage {
WebDriver driver;
UserOperations userOperation;

public ElectronicsPage(WebDriver driver){
	this.driver = driver;
	userOperation = new UserOperations(driver);
	PageFactory.initElements(driver, this);
}

@FindBy(xpath = "//span[contains(text(),'Mobiles']")
public WebElement mobiles;

public void exploreElectronicProduct(WebElement el){
	userOperation.clickElement(el);
}


}
