package helpers;

import org.openqa.selenium.WebDriver;

import pages.FlipkartHomePage;

public class FlipkartHomeHelper {
	
	public WebDriver driver;
	public FlipkartHomePage flipKartHomePage;
	
public FlipkartHomeHelper(WebDriver driver){
	this.driver = driver;
	flipKartHomePage =  new FlipkartHomePage(driver);
}

public void selectProduct(String product){
	switch (product)
	{
	case "Electronics":
		flipKartHomePage.clickProductCategory(flipKartHomePage.Electronics);;
	case "TV&Appliances":
		flipKartHomePage.clickProductCategory(flipKartHomePage.TvApplicances);
	case "Men" :
		flipKartHomePage.clickProductCategory(flipKartHomePage.men);
	case "Women" :
		flipKartHomePage.clickProductCategory(flipKartHomePage.women);
		
	}	
}
}
