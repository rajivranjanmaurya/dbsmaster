package runnerClass;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import stepDefinitions.BrowserConfiguration;
//import stepDefinitions.BrowserInitialization;

@RunWith(Cucumber.class)
@CucumberOptions(features = "Features",glue={"stepDefinitions"},
plugin = {"json:target/Destination/cucumber.json"}, monochrome=true, tags={"@regression,@smoke"})


public class RunnerTest {
	
	@BeforeClass
	public static void beforeCls(){		
		System.out.println("Before class function");		
		String browser = "chrome";
		BrowserConfiguration.intentiateBrowser(browser);			
	}
	
	@AfterClass
	public static void closeBrowser(){
		BrowserConfiguration.driver.quit();
System.out.println("Inside TestRunner");
	}
}
