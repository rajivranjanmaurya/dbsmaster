Feature:
Validate Login Functionality

Scenario: Validate positive Scenario
Given: User is on Flipkart Login Page
When: User logs in with valid "UserId" and "Password"
Then: User logged in successfully

Examples#
|UserID|Password|
|rajivranjanmaurya@gmail.com|rajiv123|

Scenario: Validate negative scenarios
Given: User is on Flipkart Login Page
When: User logs in with invalid "UserId" and "Password"
Then: User is unable to login

Examples#
|UserId|Password|
|Alok|12312|
|sona|234234|
|mona|werwer|